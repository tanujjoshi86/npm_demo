﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NPM_Demo.Startup))]
namespace NPM_Demo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
